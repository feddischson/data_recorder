// MIT License
//
// Copyright (c) 2019 Christian Haettich <feddischson@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>

#include "data_recorder.hpp"

namespace {

class Log_Test : public ::testing::Test {};

TEST_F(Log_Test, ring_buffer_counters) {
  struct Test_Recorder
      : Data_Recorder::Recorder<std::int32_t, 2, 4, true, true> {
  } r;

  EXPECT_EQ(r.idx_cnt, 0);
  EXPECT_EQ(r.size_cnt, 0);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 1);
  EXPECT_EQ(r.size_cnt, 1);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 2);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 3);
  EXPECT_EQ(r.size_cnt, 3);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 0);
  EXPECT_EQ(r.size_cnt, 4);
  EXPECT_EQ(r.is_full(), true);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 1);
  EXPECT_EQ(r.size_cnt, 4);
  EXPECT_EQ(r.is_full(), true);
}

TEST_F(Log_Test, non_ring_buffer_counters) {
  struct Test_Recorder
      : Data_Recorder::Recorder<std::int32_t, 2, 4, false, true> {
  } r;

  EXPECT_EQ(r.idx_cnt, 0);
  EXPECT_EQ(r.size_cnt, 0);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 1);
  EXPECT_EQ(r.size_cnt, 1);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 2);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 3);
  EXPECT_EQ(r.size_cnt, 3);
  EXPECT_EQ(r.is_full(), false);

  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  EXPECT_EQ(r.idx_cnt, 4);
  EXPECT_EQ(r.size_cnt, 4);
  EXPECT_EQ(r.is_full(), true);

  EXPECT_EQ(DATA_RECORD_NEXT(r), false);
  EXPECT_EQ(r.idx_cnt, 4);
  EXPECT_EQ(r.size_cnt, 4);
  EXPECT_EQ(r.is_full(), true);
}
TEST_F(Log_Test, streaming_csv_all) {
  // test both cases - ring-buffer and non-ring-buffer
  struct Test_Recorder1
      : Data_Recorder::Recorder<std::int32_t, 2, 6, false, true> {
  } r1;
  struct Test_Recorder2
      : Data_Recorder::Recorder<std::int32_t, 2, 6, true, true> {
  } r2;

  for (int i = 0; i < 4; i++) {
    DATA_RECORD(r1, 0, 10 + i);
    DATA_RECORD(r1, 1, 20 + i);
    EXPECT_EQ(DATA_RECORD_NEXT(r1), true);
    DATA_RECORD(r2, 0, 10 + i);
    DATA_RECORD(r2, 1, 20 + i);
    EXPECT_EQ(DATA_RECORD_NEXT(r2), true);
  }
  std::stringstream res1_s;
  std::stringstream res2_s;
  std::string exp_s = "10,20\n11,21\n12,22\n13,23";

  r1.write(res1_s, Data_Recorder::Format::CSV);
  r2.write(res2_s, Data_Recorder::Format::CSV);
  EXPECT_EQ(res1_s.str(), exp_s);
  EXPECT_EQ(res2_s.str(), exp_s);
  EXPECT_EQ(r1.idx_cnt, 0);
  EXPECT_EQ(r1.size_cnt, 0);
  EXPECT_EQ(r2.idx_cnt, 0);
  EXPECT_EQ(r2.size_cnt, 0);
}

TEST_F(Log_Test, streaming_csv_tail_1) {
  // test both cases - ring-buffer and non-ring-buffer
  struct Test_Recorder1
      : Data_Recorder::Recorder<std::int32_t, 2, 6, false, true> {
  } r1;
  struct Test_Recorder2
      : Data_Recorder::Recorder<std::int32_t, 2, 6, true, true> {
  } r2;

  for (int i = 0; i < 4; i++) {
    DATA_RECORD(r1, 0, 10 + i);
    DATA_RECORD(r1, 1, 20 + i);
    EXPECT_EQ(DATA_RECORD_NEXT(r1), true);
    DATA_RECORD(r2, 0, 10 + i);
    DATA_RECORD(r2, 1, 20 + i);
    EXPECT_EQ(DATA_RECORD_NEXT(r2), true);
  }
  std::stringstream res1_s;
  std::stringstream res2_s;
  std::string exp_s = "12,22\n13,23";

  r1.write(res1_s, Data_Recorder::Format::CSV, 2);
  r2.write(res2_s, Data_Recorder::Format::CSV, 2);
  EXPECT_EQ(res1_s.str(), exp_s);
  EXPECT_EQ(res2_s.str(), exp_s);
  EXPECT_EQ(r1.idx_cnt, 2);
  EXPECT_EQ(r1.size_cnt, 2);
  EXPECT_EQ(r2.idx_cnt, 2);
  EXPECT_EQ(r2.size_cnt, 2);
}

TEST_F(Log_Test, streaming_csv_tail_2) {
  struct Test_Recorder
      : Data_Recorder::Recorder<std::int32_t, 2, 6, true, true> {
  } r;

  for (int i = 0; i < 8; i++) {
    DATA_RECORD(r, 0, 10 + i);
    DATA_RECORD(r, 1, 20 + i);
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 6);

  std::stringstream res_s;
  std::string exp_s = "16,26\n17,27";

  r.write(res_s, Data_Recorder::Format::CSV, 2);
  EXPECT_EQ(res_s.str(), exp_s);
  EXPECT_EQ(r.idx_cnt, 0);
  EXPECT_EQ(r.size_cnt, 4);
}

TEST_F(Log_Test, streaming_csv_tail_3) {
  struct Test_Recorder
      : Data_Recorder::Recorder<std::int32_t, 2, 6, true, true> {
  } r;

  for (int i = 0; i < 8; i++) {
    DATA_RECORD(r, 0, 10 + i);
    DATA_RECORD(r, 1, 20 + i);
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 6);

  std::stringstream res_s;
  std::string exp_s = "15,25\n16,26\n17,27";

  r.write(res_s, Data_Recorder::Format::CSV, 3);
  EXPECT_EQ(res_s.str(), exp_s);
  EXPECT_EQ(r.idx_cnt, 5);
  EXPECT_EQ(r.size_cnt, 3);
}
TEST_F(Log_Test, streaming_bin_all) {
  struct Test_Recorder : Data_Recorder::Recorder<char, 2, 6, false, true> {
  } r;

  for (char i = 0; i < 4; ++i) {
    DATA_RECORD(r, 0, static_cast<char>('a' + i));
    DATA_RECORD(r, 1, static_cast<char>('1' + i));
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  std::stringstream res_s;
  std::string exp_s = "a1b2c3d4";

  r.write(res_s, Data_Recorder::Format::Bin);
  EXPECT_EQ(res_s.str(), exp_s);
  EXPECT_EQ(r.idx_cnt, 0);
  EXPECT_EQ(r.size_cnt, 0);
}

TEST_F(Log_Test, streaming_bin_tail1) {
  struct Test_Recorder : Data_Recorder::Recorder<char, 2, 6, false, true> {
  } r;

  for (int i = 0; i < 4; i++) {
    DATA_RECORD(r, 0, static_cast<char>('a' + i));
    DATA_RECORD(r, 1, static_cast<char>('1' + i));
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  std::stringstream res_s;
  std::string exp_s = "c3d4";

  r.write(res_s, Data_Recorder::Format::Bin, 2);
  EXPECT_EQ(res_s.str(), exp_s);
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 2);
}

TEST_F(Log_Test, streaming_bin_tail2) {
  struct Test_Recorder : Data_Recorder::Recorder<char, 2, 6, true, true> {
  } r;

  for (int i = 0; i < 8; i++) {
    DATA_RECORD(r, 0, static_cast<char>('a' + i));
    DATA_RECORD(r, 1, static_cast<char>('1' + i));
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 6);

  std::stringstream res_s;
  std::string exp_s = "g7h8";

  r.write(res_s, Data_Recorder::Format::Bin, 2);
  EXPECT_EQ(res_s.str(), exp_s);
  EXPECT_EQ(r.idx_cnt, 0);
  EXPECT_EQ(r.size_cnt, 4);
}

TEST_F(Log_Test, streaming_bin_tail3) {
  struct Test_Recorder : Data_Recorder::Recorder<char, 2, 6, true, true> {
  } r;

  for (int i = 0; i < 8; i++) {
    DATA_RECORD(r, 0, static_cast<char>('a' + i));
    DATA_RECORD(r, 1, static_cast<char>('1' + i));
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  EXPECT_EQ(r.idx_cnt, 2);
  EXPECT_EQ(r.size_cnt, 6);

  std::stringstream res_s;
  std::string exp_s = "f6g7h8";

  r.write(res_s, Data_Recorder::Format::Bin, 3);
  EXPECT_EQ(res_s.str(), exp_s);
  EXPECT_EQ(r.idx_cnt, 5);
  EXPECT_EQ(r.size_cnt, 3);
}
TEST_F(Log_Test, init_values) {
  struct Test_Recorder : Data_Recorder::Recorder<std::uint16_t, 4, 3, true, true, 5> {
  } r;

  // use next without writing,
  // this means the initial values will be in the buffer
  for (int i = 0; i < 3; i++) {
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  std::stringstream res1_s;
  std::stringstream res2_s;
  std::string exp_s = "5,5,5,5\n5,5,5,5\n5,5,5,5";

  r.write(res1_s, Data_Recorder::Format::CSV);
  EXPECT_EQ(res1_s.str(), exp_s);
}

TEST_F(Log_Test, default_reset) {
  struct Test_Recorder
      : Data_Recorder::Recorder<std::uint16_t, 4, 3, true, true, 5> {
  } r;

  // fill everything with 0
  for (int i = 0; i < 3; i++) {
    DATA_RECORD(r, 0, static_cast<std::uint16_t>(0));
    DATA_RECORD(r, 1, static_cast<std::uint16_t>(0));
    DATA_RECORD(r, 2, static_cast<std::uint16_t>(0));
    DATA_RECORD(r, 3, static_cast<std::uint16_t>(0));
    EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  }
  std::stringstream res1_s;
  std::stringstream res2_s;
  // write data
  r.write(res1_s, Data_Recorder::Format::CSV, 100, true);

  DATA_RECORD(r, 0, static_cast<std::uint16_t>(1));
  EXPECT_EQ(DATA_RECORD_NEXT(r), true);
  r.write(res2_s, Data_Recorder::Format::CSV, true);

  std::string exp_s = "1,5,5,5";

  EXPECT_EQ(res2_s.str(), exp_s);
}

}  // namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

// vim: filetype=cpp et ts=2 sw=2 sts=2
