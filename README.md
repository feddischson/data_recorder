# Synopsis

This is a small header-only data recorder (binary data logger) library written in C++.

The main focus is to use it in embedded systems with complex signal processing.
When multiple instances are working on one sample, it is interesting to record
some processing steps for each sample.
With this library, it is possible to create a recorder instance and use it in several
other instances to record data with the `DATA_RECORD` macro.
If the processing of one sample is done, the macro `DATA_RECORD_NEXT` can be used.
Then the processing of the next sample can be done.

The recorder can work as ring-buffer or as stack.
When working as ring-buffer, old entries are overwritten. When working as stack, the buffer
is filled until the stack is full. More data will be discarded.

The library is written in order that when the recorder functionality is disabled,
the two macros `DATA_RECORD` and `DATA_RECORD_NEXT` are introducing no overhead.

The internal buffer is organized via `std::array` so no dynamic memory allocation is used.

# Example

```cpp

// Flag to enable/disable recording
static constexpr bool DO_RECORD = true;

// buffer-size and number of entries definition:
//
// The internal buffer will have the size 2x4,
// and for each sample/step, 2 entries are stored.
static constexpr unsigned REC_SIZE = 4;
static constexpr unsigned REC_N_ENTRIES = 2;

// Use the recorder as ring-buffer!
static constexpr bool REC_AS_RING_BUF = true;

// create a data-recorder with 2 columns, 4 record entries
// and use it as ring-buffer
struct Custom_Recorder
    : Data_Recorder::Recorder<int32_t,
                              REC_N_ENTRIES,
                              REC_SIZE,
                              REC_AS_RING_BUF,
                              DO_RECORD,
                              0     // 0-initialization
                              > {
  // use an enum to have some meaningful name
  enum IDX : unsigned { I1 = 0, I2 = 1 };
} r;


using IDX = Custom_Recorder::IDX;

// Fill the buffer with some data:
DATA_RECORD(r, IDX::I1, 100);
DATA_RECORD(r, IDX::I2, 110);
DATA_RECORD_NEXT(r);
DATA_RECORD(r, IDX::I1, 200);
DATA_RECORD(&r, IDX::I2, 210); // uses pointer interface
DATA_RECORD_NEXT(r);

ofstream my_dump;
my_dump.open("example_dump.bin");
r.write(my_dump, Data_Recorder::Format::Bin);


```
See `example.cpp` for the full example.
