// MIT License
//
// Copyright (c) 2019 Christian Haettich <feddischson@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <fstream>
#include <iostream>

#include "data_recorder.hpp"

using std::cout;
using std::endl;
using std::int32_t;
using std::ofstream;

// flag to enable/disable recording
static constexpr bool DO_RECORD = true;

// buffer-size and number of entries definition:
//
// The internal buffer will have the size 2x4,
// and for each sample/step, 2 entries are stored.
static constexpr unsigned REC_SIZE = 4;
static constexpr unsigned REC_N_ENTRIES = 2;

// Use the recorder as ring-buffer!
static constexpr bool REC_AS_RING_BUF = true;

// create a data-recorder with 2 columns, 4 record entries
// and use it as ring-buffer
struct Custom_Recorder
    : Data_Recorder::Recorder<int32_t, REC_N_ENTRIES, REC_SIZE, REC_AS_RING_BUF,
                              DO_RECORD> {
  // use an enum to have some meaningful name
  enum IDX : unsigned { I1 = 0, I2 = 1 };
} r;

int main(int, char* []) {
  using IDX = Custom_Recorder::IDX;

  // Fill the buffer with some data:
  DATA_RECORD(r, IDX::I1, 100);
  DATA_RECORD(r, IDX::I2, 110);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 200);
  DATA_RECORD(r, IDX::I2, 210);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 300);
  DATA_RECORD(r, IDX::I2, 310);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 400);
  DATA_RECORD(r, IDX::I2, 410);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 500);
  DATA_RECORD(r, IDX::I2, 510);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 600);
  DATA_RECORD(r, IDX::I2, 610);
  // there must be a DATA_RECORD_NEXT call after the last entry!
  DATA_RECORD_NEXT(r);

  // When using the stream-operator,
  // the internal data is dumped as CSV,
  // but internal counters are not update!
  cout << r << endl;

  // this is the same than:
  r.write(cout, Data_Recorder::Format::CSV, REC_SIZE, false);
  cout << endl;

  // the buffer is still full
  cout << "is-full?: " << r.is_full() << endl;

  // in the default-case, the internal counters are update,
  // this allows to flush the buffer and record further values
  r.write(cout, Data_Recorder::Format::CSV);
  cout << endl;

  cout << "is-full?: " << r.is_full() << endl;
  cout << "is-empty?: " << r.is_empty() << endl;

  // the ring-buffer is empty, we can record further-values
  DATA_RECORD(r, IDX::I1, 700);
  DATA_RECORD(r, IDX::I2, 710);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 800);
  DATA_RECORD(r, IDX::I2, 810);
  DATA_RECORD_NEXT(r);

  // and also write them to somewhere ...
  r.write(cout, Data_Recorder::Format::CSV);
  cout << endl;
  // the buffer is empty again

  ////
  // It is also possible to write the result
  // as binary data into a file:
  DATA_RECORD(r, IDX::I1, 0x1122);
  DATA_RECORD(r, IDX::I2, 0x3344);
  DATA_RECORD_NEXT(r);
  DATA_RECORD(r, IDX::I1, 0x4455);

  // note: also pointers can be provided!
  DATA_RECORD(&r, IDX::I2, 0x6677);
  DATA_RECORD_NEXT(r);

  ofstream my_dump;
  my_dump.open("example_dump.bin");
  r.write(my_dump, Data_Recorder::Format::Bin);
  // This results in (on a little-endian machine)
  // 00000000: 2211 0000 4433 0000 5544 0000 7766 0000
}

// vim: filetype=cpp et ts=2 sw=2 sts=2
