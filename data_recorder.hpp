// MIT License
//
// Copyright (c) 2019 Christian Haettich <feddischson@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef DATA_RECORDER_HPP
#define DATA_RECORDER_HPP

#include <array>
#include <limits>
#include <type_traits>
#include <sstream>

#ifndef DATA_RECORDER_MARK_UNREACHABLE
#ifdef __GNUC__
#define DATA_RECORDER_MARK_UNREACHABLE __builtin_unreachable()
#else
// Custom unreachable definition for non-GNU compilers
// May need to be customized!
#define DATA_RECORDER_MARK_UNREACHABLE
#endif
#endif

namespace Data_Recorder {

enum class Format { CSV, Bin };

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled = false, T Init = std::numeric_limits<T>::max()>
struct Recorder {
  static const unsigned size = Size;
  static const unsigned buffer_size = Buffer_Size;

  static_assert(Buffer_Size < (std::numeric_limits<unsigned>::max() - 1),
                "The maximum size is std::numeric_limits<unsigned>::max() - 1");

  explicit Recorder() : idx_cnt{0}, size_cnt{0}, buf{{}} {
    // loop over all entries and initialize it
    for (auto& x : buf) {
      x.fill(Init);
    }
  }

  void record(const unsigned idx, T value) {
    if (!Ring_Buf && is_full()) {
      return;
    }
    buf.at(idx_cnt).at(idx) = value;
  }

  bool next() {
    if (Ring_Buf == true) {
      if (idx_cnt == Buffer_Size - 1) {
        idx_cnt = 0;
      } else {
        ++idx_cnt;
      }
    } else {
      if (idx_cnt != Buffer_Size) {
        ++idx_cnt;
      } else {
        return false;
      }
    }
    if (size_cnt < Buffer_Size) {
      ++size_cnt;
    }
    return true;
  }

  bool is_full() { return size_cnt == Buffer_Size; }

  bool is_empty() { return size_cnt == 0; }

  void write(std::ostream& os, Format format,
             unsigned N_entries = std::numeric_limits<unsigned>::max(),
             bool update = true) {
    // ensure, that the maximum possible entries are written
    if (N_entries > size_cnt) {
      N_entries = size_cnt;
    }

    // calculate the start index
    unsigned idx;
    if (idx_cnt < N_entries) {
      idx = Buffer_Size - (N_entries - idx_cnt);
    } else {
      idx = idx_cnt - N_entries;
    }

    if (format == Format::CSV) {
      for (unsigned i_cnt = 0; i_cnt < N_entries; ++i_cnt) {
        for (unsigned i_val = 0; i_val < Size; ++i_val) {
          os << buf.at(idx).at(i_val);
          if (i_val < (Size - 1)) {
            os << ",";
          }
        }
        if (idx == (Buffer_Size - 1)) {
          idx = 0;
        } else {
          ++idx;
        }
        if (i_cnt < (N_entries - 1)) {
          os << std::endl;
        }
      }
    } else if (format == Format::Bin) {
      for (unsigned i_cnt = 0; i_cnt < N_entries; ++i_cnt) {
        os.write(reinterpret_cast<char*>(buf.at(idx).data()),
                 buf.at(idx).size() * sizeof(T));
        if (idx == (Buffer_Size - 1)) {
          idx = 0;
        } else {
          ++idx;
        }
      }
    }

    if (update) {
      // update the internal counters
      size_cnt -= N_entries;
      if (idx_cnt < N_entries) {
        idx_cnt = Buffer_Size - (N_entries - idx_cnt);
      } else {
        idx_cnt -= N_entries;
      }

      // write initial values
      idx = idx_cnt;
      for (unsigned i_cnt = 0; i_cnt < N_entries; ++i_cnt) {
        buf.at(idx).fill(Init);
        if (idx == (Buffer_Size - 1)) {
          idx = 0;
        } else {
          ++idx;
        }
      }
    }
  }

  friend std::ostream& operator<<(std::ostream& os, Recorder& r) {
    r.write(os, Format::CSV, std::numeric_limits<unsigned>::max(), false);
    return os;
  }

  unsigned idx_cnt;
  unsigned size_cnt;
  std::array<std::array<T, Size>, Buffer_Size> buf;
  static const bool enabled = Enabled;
};  // namespace Data_Recorder

// Copied from
// https://github.com/foonathan/debug_assert/blob/master/debug_assert.hpp
//
//=== regular void fake ===//
struct regular_void {
  constexpr regular_void() = default;

  // enable conversion to anything
  // conversion must not actually be used
  template <typename T>
  constexpr explicit operator T&() const noexcept {
    // doesn't matter how to get the T
    return DATA_RECORDER_MARK_UNREACHABLE, *static_cast<T*>(nullptr);
  }
};

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_record(
    Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>& h,
    const unsigned idx, T value) ->
    typename std::enable_if<Enabled == true, regular_void>::type {
  h.record(idx, value);
  return regular_void();
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_record(Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>&,
                         const unsigned, T) ->
    typename std::enable_if<Enabled == false, regular_void>::type {
  return regular_void();
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_next(
    Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>& h) ->
    typename std::enable_if<Enabled == true, bool>::type {
  return h.next();
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_next(Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>&)
    -> typename std::enable_if<Enabled == false, bool>::type {
  return false;
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_record(
    Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>* h,
    const unsigned idx, T value) ->
    typename std::enable_if<Enabled == true, regular_void>::type {
  h->record(idx, value);
  return regular_void();
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_record(
    Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>*, const unsigned, T)
    -> typename std::enable_if<Enabled == false, regular_void>::type {
  return regular_void();
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_next(
    Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>* h) ->
    typename std::enable_if<Enabled == true, bool>::type {
  return h->next();
}

template <typename T, unsigned Size, unsigned Buffer_Size, bool Ring_Buf,
          bool Enabled, T Init>
constexpr auto do_next(Recorder<T, Size, Buffer_Size, Ring_Buf, Enabled, Init>*)
    -> typename std::enable_if<Enabled == false, bool>::type {
  return false;
}

}  // namespace Data_Recorder

#define DATA_RECORD(...) Data_Recorder::do_record(__VA_ARGS__)

#define DATA_RECORD_NEXT(...) Data_Recorder::do_next(__VA_ARGS__)

#endif  // DATA_RECORDER_HPP

// vim: filetype=cpp et ts=2 sw=2 sts=2
